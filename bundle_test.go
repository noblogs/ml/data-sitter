package datasitter

import (
	"bytes"
	"os"
	"path/filepath"
	"testing"
)

var testData = []byte("test data")

func TestBundle(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	os.Mkdir(filepath.Join(dir, "src"), 0700)                       //nolint:errcheck
	os.WriteFile(filepath.Join(dir, "src", "data"), testData, 0600) //nolint:errcheck
	b, err := MakeBundle(filepath.Join(dir, "src"))
	if err != nil {
		t.Fatalf("MakeBundle: %v", err)
	}
	defer b.Remove()

	os.Mkdir(filepath.Join(dir, "unpacked"), 0700) //nolint:errcheck
	if err := b.unpack(filepath.Join(dir, "unpacked")); err != nil {
		t.Fatalf("unpack: %v", err)
	}

	data, err := os.ReadFile(filepath.Join(dir, "unpacked", "data"))
	if err != nil {
		t.Fatalf("reading unpacked data: %v", err)
	}
	if !bytes.Equal(data, testData) {
		t.Fatalf("error in unpacked data ('%s')", data)
	}
}
