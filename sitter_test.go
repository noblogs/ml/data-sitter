package datasitter

import (
	"context"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
	"time"
)

const (
	testCorpusName = "test-corpus"
	testSecret     = "test-secret"
)

type testEnv struct {
	dir         string
	client      *Client
	coordSrv    *httptest.Server
	coordCorpus *Corpus
}

func mkTestEnv(t *testing.T) *testEnv {
	t.Helper()

	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}

	coordCorpus, err := NewCorpus(
		testCorpusName,
		filepath.Join(dir, "coordinator"),
		false)
	if err != nil {
		t.Fatalf("NewCorpus(coordinator): %v", err)
	}
	coord := NewSingleCorpusCoordinator(coordCorpus, testSecret)
	coordSrv := httptest.NewServer(coord)

	client := NewCoordinatorClient(coordSrv.URL, testSecret)

	return &testEnv{
		dir:         dir,
		coordSrv:    coordSrv,
		coordCorpus: coordCorpus,
		client:      client,
	}
}

func (e *testEnv) Close() {
	e.coordSrv.Close()
	os.RemoveAll(e.dir)
}

func (e *testEnv) checkLatestVersion(t *testing.T, cur *Version) *Version {
	t.Helper()

	v, err := e.client.checkLatestVersion(
		context.Background(),
		testCorpusName,
		cur,
	)
	if err != nil {
		t.Fatalf("checkLatestVersion: %v", err)
	}
	return v
}

func (e *testEnv) mkBundle(t *testing.T, data string) *Bundle {
	t.Helper()

	bundlePath := filepath.Join(e.dir, "test-bundle")
	os.Mkdir(bundlePath, 0700)                                          //nolint:errcheck
	os.WriteFile(filepath.Join(bundlePath, "data"), []byte(data), 0600) //nolint:errcheck
	bundle, err := MakeBundle(bundlePath)
	if err != nil {
		t.Fatalf("MakeBundle: %v", err)
	}
	return bundle
}

func (e *testEnv) uploadNewVersion(t *testing.T, bundle *Bundle, setLive bool) *Version {
	t.Helper()

	defer bundle.Remove()

	v, err := e.client.UploadNewVersion(
		context.Background(),
		testCorpusName,
		bundle,
		setLive,
	)
	if err != nil {
		t.Fatalf("UploadNewVersion: %v", err)
	}
	if v == nil {
		t.Fatalf("UploadNewVersion: return value is nil")
	}
	if v.ID.IsZero() {
		t.Fatalf("UploadNewVersion returned a Version with nil ID: %+v", *v)
	}
	return v
}

func (e *testEnv) downloadVersionToFile(t *testing.T, v *Version) string {
	t.Helper()

	outPath := filepath.Join(e.dir, "output")
	if err := e.client.downloadVersionToFile(
		context.Background(),
		testCorpusName,
		v,
		outPath,
	); err != nil {
		t.Fatalf("downloadVersionToFile: %v", err)
	}

	if _, err := os.Stat(outPath); err != nil {
		t.Fatalf("output file does not exist: %v", err)
	}

	return outPath
}

func (e *testEnv) setLiveVersion(t *testing.T, id VersionID, safe bool) error {
	t.Helper()

	err := e.client.SetLiveVersion(
		context.Background(),
		testCorpusName,
		id,
		safe,
	)
	// Horrible check that the only accepted error is a 400.
	if err != nil && err.Error() != "HTTP status code 400" {
		t.Fatalf("SetLiveVersion unexpected error: %v", err)
	}
	return err
}

func TestCoordinator_UploadNewVersion(t *testing.T) {
	env := mkTestEnv(t)
	defer env.Close()

	// Test nil latest version.
	if v := env.checkLatestVersion(t, nil); v != nil {
		t.Fatalf("oops, we got something but expected nothing: %+v", v)
	}

	// Upload a new version.
	v := env.uploadNewVersion(t, env.mkBundle(t, "42"), true)

	// Test the new latest version.
	v2 := env.checkLatestVersion(t, nil)
	if v2 == nil {
		t.Fatal("nil reply from checkLatestVersion/2")
	}
	if v2.ID != v.ID {
		t.Fatalf("v1 and v2 differ: %s / %s", v.ID, v2.ID)
	}

	// Download.
	env.downloadVersionToFile(t, v2)
}

func TestCoordinator_Cleanup(t *testing.T) {
	env := mkTestEnv(t)
	defer env.Close()

	for i := 0; i < 10; i++ {
		// Upload a new version.
		env.uploadNewVersion(t, env.mkBundle(t, "42"), true)
	}

	numVersions := len(env.coordCorpus.Versions)
	if numVersions != 2 {
		t.Fatalf("expected %d versions in corpus, found %d", 2, numVersions)
	}
}

func TestCoordinator_Cleanup_NoLiveVersions(t *testing.T) {
	env := mkTestEnv(t)
	defer env.Close()

	for i := 0; i < 10; i++ {
		// Upload a new version.
		env.uploadNewVersion(t, env.mkBundle(t, "42"), false)
	}

	numVersions := len(env.coordCorpus.Versions)
	if numVersions != 10 {
		t.Fatalf("expected %d versions in corpus, found %d", 10, numVersions)
	}
}

func TestCoordinator_Rollback(t *testing.T) {
	env := mkTestEnv(t)
	defer env.Close()

	var versions [10]*Version
	for i := 0; i < len(versions); i++ {
		// Upload a new version.
		versions[i] = env.uploadNewVersion(t, env.mkBundle(t, "42"), true)
	}

	if err := env.setLiveVersion(t, versions[0].ID, false); err == nil {
		t.Fatal("attempted to rollback 10 versions succeeded when it should have failed")
	}

	// Rollback to the previous version.
	lastVersionID := versions[len(versions)-2].ID
	if _, ok := env.coordCorpus.Versions[lastVersionID]; !ok {
		t.Fatalf("oops, version %s no longer in coordinator corpus", lastVersionID)
	}
	if err := env.setLiveVersion(t, lastVersionID, false); err != nil {
		t.Fatalf("setLiveVersion(%s): %v", lastVersionID, err)
	}
	if v, ok := env.coordCorpus.Versions[versions[len(versions)-1].ID]; !ok || !v.RolledBack {
		t.Fatalf("rolled back version does not have the RolledBack flag set (v=%+v)", v)
	}

	// At this point there is no longer a rollback-safe version
	// because the previous-previous one has been cleaned up (keep=2).
	if err := env.setLiveVersion(t, VersionID(0), true); err == nil {
		t.Fatal("setLiveVersion(safe) succeeded when it should have failed")
	}
}

func TestCoordinator_RollbackSafe(t *testing.T) {
	env := mkTestEnv(t)
	defer env.Close()

	var versions [10]*Version
	for i := 0; i < len(versions); i++ {
		// Upload a new version.
		versions[i] = env.uploadNewVersion(t, env.mkBundle(t, "42"), true)
	}

	// Rollback to the safe version.
	if err := env.setLiveVersion(t, VersionID(0), true); err != nil {
		t.Fatalf("setLiveVersion(safe): %v", err)
	}
	if v, ok := env.coordCorpus.Versions[versions[len(versions)-1].ID]; !ok || !v.RolledBack {
		t.Fatalf("rolled back version does not have the RolledBack flag set (v=%+v)", v)
	}

	// The current version should be the previous one.
	if env.coordCorpus.Live != versions[len(versions)-2].ID {
		t.Fatalf("setLiveVersion(safe) rolled back to %s instead of expected %s",
			env.coordCorpus.Live,
			versions[len(versions)-2].ID)
	}
}

type testProc struct {
	startCount int
}

func (t *testProc) Run(ctx context.Context, v *Version) error {
	t.startCount++
	<-ctx.Done()
	return nil
}

func TestSitter(t *testing.T) {
	env := mkTestEnv(t)
	defer env.Close()

	sitterCorpus, err := NewCorpus(
		testCorpusName,
		filepath.Join(env.dir, "sitter"),
		true)
	if err != nil {
		t.Fatalf("NewCorpus(sitter): %v", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	proc := new(testProc)
	errCh := make(chan error)

	go func() {
		errCh <- runSitter(ctx, env.client, sitterCorpus, proc, 100*time.Millisecond)
	}()

	// Upload a couple of new versions.
	count := 2
	var v *Version
	for i := 0; i < count; i++ {
		v = env.uploadNewVersion(t, env.mkBundle(t, "42"), true)
		time.Sleep(300 * time.Millisecond)
	}
	cancel()

	if err := <-errCh; err != nil {
		t.Fatalf("runSitter failed: %v", err)
	}

	if n := proc.startCount; n != count {
		t.Errorf("managed process started %d times, expected %d", n, count)
	}

	outPath := sitterCorpus.versionPath(v)
	if _, err := os.Stat(outPath); err != nil {
		t.Error("version directory does not exist in sitter corpus")
	}
}
