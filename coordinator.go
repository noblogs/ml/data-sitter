package datasitter

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

type Coordinator struct {
	corpora map[string]*Corpus
	secrets map[string]string
}

// NewSingleCorpusCoordinator manages a single Corpus.
func NewSingleCorpusCoordinator(corpus *Corpus, secret string) *Coordinator {
	return &Coordinator{
		corpora: map[string]*Corpus{corpus.Name: corpus},
		secrets: map[string]string{corpus.Name: secret},
	}
}

func (c *Coordinator) auth(req *http.Request, corpus string) bool {
	secret, ok := c.secrets[corpus]
	reqSecret, reqOk := parseAuthHeaderFromRequest(req)
	return ok && reqOk && secret == reqSecret
}

func (c *Coordinator) getCorpus(req *http.Request) *Corpus {
	name := req.FormValue("corpus")
	return c.corpora[name]
}

func (c *Coordinator) withCorpus(w http.ResponseWriter, req *http.Request, f func(*Corpus)) {
	corpus := c.getCorpus(req)
	if corpus == nil {
		http.NotFound(w, req)
		return
	}

	if !c.auth(req, corpus.Name) {
		http.Error(w, "Unauthorized", http.StatusForbidden)
		return
	}

	f(corpus)
}

func (c *Coordinator) handleCheckVersion(w http.ResponseWriter, req *http.Request) {
	c.withCorpus(w, req, func(corpus *Corpus) {
		remoteID, err := ParseVersionID(req.FormValue("version"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var live Version
		var ok bool
		// nolint: errcheck
		corpus.withReadonlyTx(func() error {
			live, ok = corpus.getLiveVersion()
			return nil
		})

		// If we don't have a live version yet, or the remote
		// is already at the live version, report no change.
		if !ok || remoteID >= live.ID {
			w.WriteHeader(http.StatusNotModified)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(live) //nolint:errcheck
	})
}

func (c *Coordinator) handleDownloadVersion(w http.ResponseWriter, req *http.Request) {
	c.withCorpus(w, req, func(corpus *Corpus) {
		id, err := ParseVersionID(req.FormValue("version"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		var path string

		err = corpus.withTx(func() error {
			version, ok := corpus.getVersionByID(id)
			if !ok {
				return errors.New("not found")
			}
			path = corpus.versionPath(version)
			return nil
		})
		if err != nil {
			http.NotFound(w, req)
			return
		}

		http.ServeFile(w, req, path)
	})
}

const multipartFormMemBufSize = 16 * 1024

func (c *Coordinator) handleUploadNewVersion(w http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.Error(w, "Unsupported method", http.StatusMethodNotAllowed)
		return
	}

	if err := req.ParseMultipartForm(multipartFormMemBufSize); err != nil {
		log.Printf("ParseMultipartForm error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	c.withCorpus(w, req, func(corpus *Corpus) {
		b, err := bundleFromRequest(req, "bundle", corpus.tmpPath())
		if err != nil {
			log.Printf("bundleFromRequest error: %v", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		defer b.Remove()

		err = corpus.withTx(func() error {
			if err := corpus.accept(b); err != nil {
				return err
			}
			if req.FormValue("live") == "y" {
				if err := corpus.setLiveVersion(b.Version.ID); err != nil {
					return err
				}
			}
			return nil
		})

		if err != nil {
			log.Printf("uploadNewVersion error: %v", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		log.Printf("%s: received new version %s (live=%v)", corpus.Name, b.Version.ID,
			req.FormValue("live") == "y")
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(b.Version) //nolint:errcheck
	})
}

func (c *Coordinator) handleSetLiveVersion(w http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.Error(w, "Unsupported method", http.StatusMethodNotAllowed)
		return
	}

	c.withCorpus(w, req, func(corpus *Corpus) {
		versionStr := req.FormValue("version")
		var id VersionID

		err := corpus.withTx(func() error {
			if versionStr == "safe" {
				safe, ok := corpus.getRollbackSafeVersion(nil)
				if !ok {
					return errors.New("No available rollback-safe version")
				}
				id = safe
			} else {
				var err error
				id, err = ParseVersionID(req.FormValue("version"))
				if err != nil {
					return err
				}
			}

			return corpus.setLiveVersion(id)
		})

		if err != nil {
			// This likely means trouble for someone, so we should log it.
			log.Printf("setLiveVersion(%s, %s) error: %v", corpus.Name, versionStr, err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		log.Printf("%s: set live version to %s", corpus.Name, id)
		w.WriteHeader(http.StatusOK)
	})
}

func (c *Coordinator) handleListVersions(w http.ResponseWriter, req *http.Request) {
	c.withCorpus(w, req, func(corpus *Corpus) {
		var versions []Version

		// nolint: errcheck
		corpus.withReadonlyTx(func() error {
			versions = derefVersions(corpus.getAllVersionsSortedByID())
			return nil
		})

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(versions) //nolint:errcheck
	})
}

func (c *Coordinator) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	switch req.URL.Path {
	case "/api/upload":
		c.handleUploadNewVersion(w, req)
	case "/api/download-version":
		c.handleDownloadVersion(w, req)
	case "/api/check-version":
		c.handleCheckVersion(w, req)
	case "/api/set-live-version":
		c.handleSetLiveVersion(w, req)
	case "/api/list-versions":
		c.handleListVersions(w, req)
	default:
		http.NotFound(w, req)
	}
}

func bundleFromRequest(req *http.Request, key, tmpDir string) (*Bundle, error) {
	f, _, err := req.FormFile(key)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	b := Bundle{
		Version: &Version{
			ID:         newVersionID(),
			CreatedAt:  time.Now(),
			ReceivedAt: time.Now(),
		},
	}

	// Skip a copy if the input is already file-backed.
	switch ff := f.(type) {
	case *os.File:
		b.Path = ff.Name()
	default:
		tmpf, err := copyToTemporaryFile(f, tmpDir)
		if err != nil {
			return nil, err
		}
		b.Path = tmpf
	}

	// Size detection also works as data validation.
	sz, err := b.getSize()
	if err != nil {
		return nil, err
	}
	b.Version.Size = sz

	return &b, nil
}

func newVersionID() VersionID {
	// For normal version generation just use the nanosecond
	// timestamp (big endian).
	return VersionID(time.Now().UnixNano())
}

func copyToTemporaryFile(f io.Reader, dir string) (string, error) {
	tmpf, err := os.CreateTemp(dir, "bundle.*")
	if err != nil {
		return "", err
	}
	defer tmpf.Close()
	if _, err = io.Copy(tmpf, f); err != nil {
		os.Remove(tmpf.Name())
		return "", err
	}
	return tmpf.Name(), nil
}
