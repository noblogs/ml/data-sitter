package datasitter

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"syscall"
	"text/template"
	"time"
)

// The "sitter" (data push client) is implemented as a pipeline with a
// series of stages connected by Go channels: Puller -> Downloader ->
// Runner. Each stage, once triggered, will perform its task and keep
// retrying until it succeeds, passing along the message to the next
// stage.
//
// This simple implementation allows restartable downloads, and
// transparently implements the correct semantics for the managed
// application process.
//
// We use value channels throughout to avoid inadvertedly sharing
// state between stages.

type coordinatorStub interface {
	checkLatestVersion(context.Context, string, *Version) (*Version, error)
	downloadVersionToFile(context.Context, string, *Version, string) error
}

// Puller periodically polls the coordinator for the current 'live'
// version of the corpus.
type Puller struct {
	corpus   *Corpus
	ch       chan Version
	client   coordinatorStub
	interval time.Duration
}

func (p *Puller) run(ctx context.Context) {
	defer close(p.ch)

	// Initialize the pipeline with the saved state, if any.
	var live Version
	var ok bool

	// nolint: errcheck
	p.corpus.withReadonlyTx(func() error {
		live, ok = p.corpus.getLiveVersion()
		return nil
	})
	if ok {
		p.ch <- live
	}

	tick := time.NewTicker(p.interval)
	defer tick.Stop()

	for {
		select {
		case <-ctx.Done():
			return

		case <-tick.C:
			vctx, cancel := context.WithTimeout(ctx, checkTimeout)
			var curVersionPtr *Version
			if !live.ID.IsZero() {
				curVersionPtr = &live
			}
			newVersion, err := p.client.checkLatestVersion(vctx, p.corpus.Name, curVersionPtr)
			cancel()
			if err != nil {
				log.Printf("checkLatestVersion() error: %v", err)
				continue
			}
			if newVersion == nil {
				continue
			}

			log.Printf("received new version, %s -> %s",
				live.ID.String(), newVersion.ID.String())

			live = *newVersion
			p.ch <- live
		}
	}
}

// Downloader attempts to download version data from the coordinator
// (if not already locally present).
type Downloader struct {
	corpus *Corpus
	dir    string
	client coordinatorStub

	inCh  chan Version
	outCh chan Version
}

func (d *Downloader) downloadFileName(v *Version) string {
	return filepath.Join(d.dir, fmt.Sprintf("download.%s", v.ID.String()))
}

func (d *Downloader) doDownload(ctx context.Context, v *Version) (*Bundle, error) {
	path := d.downloadFileName(v)

	if err := d.client.downloadVersionToFile(ctx, d.corpus.Name, v, path); err != nil {
		return nil, err
	}

	return &Bundle{
		Path:    path,
		Version: v,
	}, nil
}

func (d *Downloader) download(ctx context.Context, v *Version) (*Bundle, error) {
	for {
		dctx, cancel := context.WithTimeout(ctx, downloadTimeout)
		bundle, err := d.doDownload(dctx, v)
		cancel()

		if err == nil {
			return bundle, nil
		}

		log.Printf("error downloading %s: %v", v.ID.String(), err)
		if err := sleepContext(ctx, downloadRetryInterval); err != nil {
			return nil, err
		}
	}
}

func (d *Downloader) process(ctx context.Context, v *Version) error {
	return d.corpus.withTx(func() error {
		if !d.corpus.containsVersion(v) {
			bundle, err := d.download(ctx, v)
			if err != nil {
				return err
			}
			defer bundle.Remove()

			if err := d.corpus.accept(bundle); err != nil {
				return fmt.Errorf("accept() error: %w", err)
			}
		}

		if err := d.corpus.setLiveVersion(v.ID); err != nil {
			return err
		}

		d.outCh <- *v

		return nil
	})
}

func (d *Downloader) run() {
	defer close(d.outCh)

	for {
		err := genericAsyncRunner(d.inCh, d.process)
		if err != nil {
			log.Printf("download error: %v", err)
			continue
		}

		return
	}
}

type templateCommand struct {
	corpus *Corpus
	cmd    *template.Template
}

func newTemplateCommand(corpus *Corpus, cmd string) (*templateCommand, error) {
	tpl, err := template.New("").Parse(cmd)
	if err != nil {
		return nil, fmt.Errorf("error in command template: %w", err)
	}
	return &templateCommand{
		corpus: corpus,
		cmd:    tpl,
	}, nil
}

func (c *templateCommand) buildCmd(ctx context.Context, v *Version) (*exec.Cmd, error) {
	var buf bytes.Buffer
	if err := c.cmd.Execute(&buf, map[string]any{
		"Path":    c.corpus.versionPath(v),
		"Version": v,
	}); err != nil {
		return nil, err
	}

	cmd := exec.CommandContext(ctx, "/bin/sh", "-c", buf.String())
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	// Run the child in a new process group, so that we can safely
	// terminate not just the child process but any other process
	// that it might spawn.
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}

	return cmd, nil
}

func (c *templateCommand) Run(ctx context.Context, v *Version) error {
	cmd, err := c.buildCmd(ctx, v)
	if err != nil {
		return err
	}

	return cmd.Run()
}

// Interface for something that can be run with a controlling Context
// and a specififc Version.
type managedProc interface {
	Run(context.Context, *Version) error
}

// The Runner runs a managed process whenever a new live version is
// succesfully downloaded.
type Runner struct {
	corpus *Corpus
	proc   managedProc
	ch     chan Version
}

func (r *Runner) run() error {
	return genericAsyncRunner(r.ch, r.proc.Run)
}

// Run the data sitter.
func Run(ctx context.Context, corpus *Corpus, coordinatorURI, secret, cmd string) error {
	proc, err := newTemplateCommand(corpus, cmd)
	if err != nil {
		return err
	}

	return runSitter(ctx, NewCoordinatorClient(coordinatorURI, secret), corpus, proc, 1*time.Minute)
}

// Build the sitter pipeline.
func runSitter(ctx context.Context, client coordinatorStub, corpus *Corpus, proc managedProc, pollInterval time.Duration) error {
	puller := &Puller{
		corpus:   corpus,
		client:   client,
		ch:       make(chan Version),
		interval: pollInterval,
	}
	go puller.run(ctx)

	dl := &Downloader{
		corpus: corpus,
		client: client,
		dir:    corpus.tmpPath(),
		inCh:   puller.ch,
		outCh:  make(chan Version),
	}
	go dl.run()

	runner := &Runner{
		corpus: corpus,
		proc:   proc,
		ch:     dl.outCh,
	}
	return runner.run()
}

func sleepContext(ctx context.Context, d time.Duration) error {
	t := time.NewTimer(d)
	defer t.Stop()
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-t.C:
		return nil
	}
}

// The genericAsyncRunner function will listen on a channel for
// Versions and it will call an (interruptible) function 'f' with each
// received value. If 'f' is still running when a new value comes in,
// its controlling Context is canceled.
//
// If 'f' returns an error, this function will stop listening and
// return that error immediately.
//
// nolint:govet
func genericAsyncRunner(ch <-chan Version, f func(context.Context, *Version) error) error {
	var running bool
	var cancel context.CancelFunc
	var doneCh chan struct{}
	var errCh chan error

	// The linter is still unhappy because it can't track the
	// logic that keeps 'cancel' set.
	defer func() {
		if cancel != nil {
			cancel()
		}
	}()

	for {
		select {
		case err := <-errCh:
			if err != nil {
				return fmt.Errorf("unexpected process failure: %w", err)
			}

		case <-doneCh:
			running = false
			doneCh = nil
			errCh = nil
			cancel()

		case v, ok := <-ch:
			if running {
				log.Printf("canceling current process")
				cancel()
				<-doneCh
			}

			if !ok {
				return nil
			}

			var ctx context.Context
			ctx, cancel = context.WithCancel(context.Background())
			doneCh = make(chan struct{})
			errCh = make(chan error, 1)
			running = true

			go func() {
				if err := f(ctx, &v); err != nil {
					errCh <- fmt.Errorf("%s: %w", v.ID, err)
				}
				close(doneCh)
				close(errCh)
			}()
		}
	}

}
