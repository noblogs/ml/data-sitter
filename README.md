data-sitter
===

Software suite to distribute large datasets to multiple instances of
serving jobs in a coordinated fashion.

The basic idea is: we run a *coordinator*, where new datasets are
uploaded to. Various *sitters* then talk to the coordinator to
download the dataset, and spawn a command (usually to start a service)
with it. Whenever the dataset is updated, the sitter restarts the
command with the new data version.

## Overview

A *corpus* is a single logical dataset, identified by a name, which
can have multiple subsequent immutable *versions*, each identified by
a monotonically increasing version number. For simplicity, version
numbers are currently timestamps.

The coordinator currently manages a single corpus, we're considering
for future development whether it makes more sense to run a single
coordinator supporting multiple corpora, or to have one isolated
coordinator per corpus.

## Authentication

At the moment all requests (internal and external) need to be
authenticated using a shared secret, passed in the HTTP Authorization
header with the *Bearer* mechanism.

## Usage

As an example, let's run a simple local coordinator/sitter scenario.

Run the coordinator with (using `$CORPUS` and `$SECRET` as
placeholders for the corpus name and the shared secret respectively):

```
data-sitter coordinator --name=$CORPUS --storage-dir=/tmp/coordinator/$CORPUS \
    --secret=$SECRET
```

The coordinator should now be listening on port 4161.

Our "application" will be a simple static HTTP server using a stock
Python 3 module, which will serve the corpus contents without any
further processing. The application runs under data-sitter:

```
data-sitter run --name=$CORPUS --coordinator=http://localhost:4161 \
    --storage-dir=/tmp/sitter/$CORPUS \
    --secret=$SECRET 'python3 -m http.server -d {{.Path}}'
```

The HTTP server will listen on port 8000. But right now you'll note
that this command does not do anything, and the Python HTTP server is
not started - that's because we have not uploaded any data to the
coordinator yet.

So let's create a test dataset, in its own subdirectory, which will
contain just a plain HTML index page for demonstration purposes:

```
mkdir mydata
echo '<html><body>Hello world</body></html>' > mydata/index.html
```

Now upload it to the coordinator:

```
data-sitter upload --name=$CORPUS --coordinator=http://localhost:4161 \
    --secret=$SECRET mydata
```

With this, the managed application should be started and serving our
test index page, which you can check with:

```
curl -s http://localhost:8000/index.html
```

Running "data-sitter upload" again with a modified data should result
in the HTTP server restarting in order to serve the updated content.

### Rolling back to a previous version

Coordinator and clients clean up old versions quite aggressively, to
conserve disk space, but they keep a few old versions around in case
there is a need to roll back the corpus to a previous version. This
can be achieved with the "data-sitter set-live-version" command:

```
data-sitter set-live-version --name=$CORPUS --version=$OLD_VERSION ...
```

Most importantly, a *rollback-safe* version is always kept around:
this is defined as the most recent corpus version that was live at
some point, and that was not rolled back (i.e. it was replaced by a
later version, not an earlier one). Since this is a convenient
emergency procedure, it has its own special option *--safe*:

```
data-sitter set-live-version --name=$CORPUS --safe ...
```

## Monitoring

Since the tool is based on asynchronous polling, it has no concept of
an individual "push" as a self-contained process, so it can't tell you
if the version just uploaded actually made it to the clients without
errors or not.

For this reason, you're supposed to monitor *the clients* somehow (for
instance by exporting their active corpus version ID). Right now this
instrumentation needs to be provided by your application, but we might
consider adding an HTTP exporter to the "data-sitter" tool itself.
