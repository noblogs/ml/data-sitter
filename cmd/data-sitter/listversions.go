package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"time"

	"github.com/google/subcommands"
)

type lsCommand struct {
	clientCommandBase

	name string
}

func (c *lsCommand) Name() string     { return "ls" }
func (c *lsCommand) Synopsis() string { return "List known versions" }
func (c *lsCommand) Usage() string {
	return `ls:
	List versions known to the coordinator.

`
}

func (c *lsCommand) SetFlags(f *flag.FlagSet) {
	c.clientCommandBase.SetFlags(f)
	f.StringVar(&c.name, "name", "", "corpus name")
}

func (c *lsCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	if c.name == "" {
		log.Printf("Must specify --name")
		return subcommands.ExitUsageError
	}
	if c.coordinatorURI == "" {
		log.Printf("Must specify --coordinator")
		return subcommands.ExitUsageError
	}

	versions, err := c.Client().ListVersions(ctx, c.name)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	for _, v := range versions {
		var s string
		if v.RolledBack {
			s = "[rolled back]"
		}
		fmt.Printf(
			"%-20s %20d %16s %16s %s\n",
			v.ID,
			v.Size,
			formatTimestamp(v.CreatedAt),
			formatTimestamp(v.SetLiveAt),
			s,
		)
	}

	return subcommands.ExitSuccess
}

func formatTimestamp(t time.Time) string {
	if t.IsZero() {
		return ""
	}
	return t.Format(time.RFC3339)
}

func init() {
	subcommands.Register(&lsCommand{}, "")
}
