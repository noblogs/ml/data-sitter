package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"

	datasitter "git.autistici.org/noblogs/ml/data-sitter"
	"github.com/google/subcommands"
)

type uploadCommand struct {
	clientCommandBase

	name          string
	doNotMarkLive bool
}

func (c *uploadCommand) Name() string     { return "upload" }
func (c *uploadCommand) Synopsis() string { return "Upload a new dataset version" }
func (c *uploadCommand) Usage() string {
	return `upload <PATH>:
	Builds a dataset bundle from files below PATH, and uploads it to
	the data-sitter coordinator.

`
}

func (c *uploadCommand) SetFlags(f *flag.FlagSet) {
	c.clientCommandBase.SetFlags(f)
	f.StringVar(&c.name, "name", "", "corpus name")
	f.BoolVar(&c.doNotMarkLive, "no-live", false, "do not mark the new version as 'live'")
}

func (c *uploadCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}

	if c.name == "" {
		log.Printf("Must specify --name")
		return subcommands.ExitUsageError
	}
	if c.coordinatorURI == "" {
		log.Printf("Must specify --coordinator")
		return subcommands.ExitUsageError
	}

	log.Printf("creating dataset bundle...")
	b, err := datasitter.MakeBundle(f.Arg(0))
	if err != nil {
		log.Fatalf("could not create bundle: %v", err)
	}
	defer b.Remove()

	log.Printf("uploading bundle...")
	v, err := c.Client().UploadNewVersion(ctx, c.name, b, !c.doNotMarkLive)
	if err != nil {
		log.Fatalf("upload error: %v", err)
	}

	out, err := json.MarshalIndent(v, "", "    ")
	if err != nil {
		log.Fatalf("error encoding JSON output: %v", err)
	}
	fmt.Printf("%s\n", out)
	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&uploadCommand{}, "")
}
