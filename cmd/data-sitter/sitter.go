package main

import (
	"context"
	"errors"
	"flag"
	"log"
	"path/filepath"
	"strings"

	datasitter "git.autistici.org/noblogs/ml/data-sitter"
	"github.com/google/subcommands"
)

var defaultSitterCorpusStorageDir = "/var/lib/data-sitter/runtime"

type runCommand struct {
	clientCommandBase

	name string
	dir  string
}

func (c *runCommand) Name() string     { return "run" }
func (c *runCommand) Synopsis() string { return "Run a command with current dataset" }
func (c *runCommand) Usage() string {
	return `run <CMD>:
	Runs CMD with the 'live' data corpus obtained by the coordinator. When the
	live version changes, the command is restarted with the new data. The given
	command must not fork into the background.

	The current version parameters can be substituted into CMD using Go template
	syntax, e.g. with '{{.Path}}' (the dataset path) or '{{.Version.ID}}' (the
	live version identifier).

	Note that CMD is passed to a shell, so take care with quoting.

`
}

func (c *runCommand) SetFlags(f *flag.FlagSet) {
	c.clientCommandBase.SetFlags(f)
	f.StringVar(&c.name, "name", "", "corpus name")
	f.StringVar(&c.dir, "storage-dir", "", "corpus storage `path`")
}

func (c *runCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() == 0 {
		log.Printf("Not enough arguments")
		return subcommands.ExitUsageError
	}

	if c.name == "" {
		log.Printf("Must specify --name")
		return subcommands.ExitUsageError
	}
	if c.coordinatorURI == "" {
		log.Printf("Must specify --coordinator")
		return subcommands.ExitUsageError
	}

	dir := c.dir
	if dir == "" {
		dir = filepath.Join(defaultSitterCorpusStorageDir, c.name)
	}

	cmd := strings.Join(f.Args(), " ")

	corpus, err := datasitter.NewCorpus(c.name, dir, true)
	if err != nil {
		log.Fatal(err)
	}

	err = datasitter.Run(ctx, corpus, c.coordinatorURI, c.secret, cmd)
	if err != nil && !errors.Is(err, context.Canceled) {
		log.Fatal(err)
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&runCommand{}, "")
}
