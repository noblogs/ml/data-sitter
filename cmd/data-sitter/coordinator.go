package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	datasitter "git.autistici.org/noblogs/ml/data-sitter"
	"github.com/google/subcommands"
)

var defaultCoordinatorCorpusStorageDir = "/var/lib/data-sitter/coordinator"

type coordCommand struct {
	addr   string
	name   string
	dir    string
	secret string
}

func (c *coordCommand) Name() string     { return "coordinator" }
func (c *coordCommand) Synopsis() string { return "Run the coordinator process" }
func (c *coordCommand) Usage() string {
	return `coordinator:
	Run the coordinator process.

`
}

func (c *coordCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.addr, "addr", ":4161", "`address` to listen on")
	f.StringVar(&c.name, "name", "", "corpus name")
	f.StringVar(&c.dir, "storage-dir", "", "corpus storage `path`")
	f.StringVar(&c.secret, "secret", "", "shared secret for HTTP authentication")
	f.Var(newFileStringFlag(&c.secret), "secret-file", "shared secret for HTTP authentication (read from file)")
}

func (c *coordCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 0 {
		log.Printf("Wrong number of arguments")
		return subcommands.ExitUsageError
	}

	if c.name == "" {
		log.Printf("Must specify --name")
		return subcommands.ExitUsageError
	}

	dir := c.dir
	if dir == "" {
		dir = filepath.Join(defaultCoordinatorCorpusStorageDir, c.name)
	}
	if err := os.MkdirAll(dir, 0700); err != nil {
		log.Fatalf("error creating storage directory: %v", err)
	}

	corpus, err := datasitter.NewCorpus(c.name, dir, false)
	if err != nil {
		log.Fatal(err)
	}

	coord := datasitter.NewSingleCorpusCoordinator(corpus, c.secret)

	// Run a simple HTTP server whose lifetime is controlled by the Context.
	srv := &http.Server{
		Addr:              c.addr,
		Handler:           coord,
		ReadHeaderTimeout: 10 * time.Second,
		IdleTimeout:       10 * time.Minute,
	}
	go func() {
		<-ctx.Done()
		srv.Close()
	}()

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&coordCommand{}, "")
}
