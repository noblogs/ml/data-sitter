package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	datasitter "git.autistici.org/noblogs/ml/data-sitter"
	"github.com/google/subcommands"
)

func init() {
	subcommands.Register(subcommands.HelpCommand(), "help")
	subcommands.Register(subcommands.CommandsCommand(), "help")
	subcommands.Register(subcommands.FlagsCommand(), "help")
}

// Utility class to set a flag value to the contents of a file.
type fileStringFlag struct {
	s *string
}

func newFileStringFlag(s *string) *fileStringFlag {
	return &fileStringFlag{s: s}
}

func (f *fileStringFlag) String() string {
	if f.s == nil {
		return ""
	}
	return *f.s
}

func (f *fileStringFlag) Set(path string) error {
	data, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	*f.s = string(data)
	return nil
}

type versionIDFlag struct {
	v *datasitter.VersionID
}

func newVersionIDFlag(v *datasitter.VersionID) *versionIDFlag {
	return &versionIDFlag{v: v}
}

func (f *versionIDFlag) String() string {
	if f.v == nil {
		return ""
	}
	return f.v.String()
}

func (f *versionIDFlag) Set(value string) error {
	id, err := datasitter.ParseVersionID(value)
	if err != nil {
		return err
	}

	*f.v = id
	return nil
}

// Generic client command.
type clientCommandBase struct {
	coordinatorURI string
	secret         string
}

func (c *clientCommandBase) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.coordinatorURI, "coordinator", "", "coordinator server `URI`")
	f.StringVar(&c.secret, "secret", "", "shared secret for HTTP authentication")
	f.Var(newFileStringFlag(&c.secret), "secret-file", "shared secret for HTTP authentication (read from file)")
}

func (c *clientCommandBase) Client() *datasitter.Client {
	return datasitter.NewCoordinatorClient(c.coordinatorURI, c.secret)
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())

	stopCh := make(chan os.Signal, 1)
	go func() {
		<-stopCh
		cancel()
	}()
	signal.Notify(stopCh, syscall.SIGINT, syscall.SIGTERM)

	os.Exit(int(subcommands.Execute(ctx)))
}
