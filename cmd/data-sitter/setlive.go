package main

import (
	"context"
	"flag"
	"log"

	datasitter "git.autistici.org/noblogs/ml/data-sitter"
	"github.com/google/subcommands"
)

type setLiveCommand struct {
	clientCommandBase

	name    string
	version datasitter.VersionID
	safe    bool
}

func (c *setLiveCommand) Name() string     { return "set-live" }
func (c *setLiveCommand) Synopsis() string { return "Set the currently 'live' version" }
func (c *setLiveCommand) Usage() string {
	return `set-live:
	Set the currently 'live' version.

`
}

func (c *setLiveCommand) SetFlags(f *flag.FlagSet) {
	c.clientCommandBase.SetFlags(f)
	f.StringVar(&c.name, "name", "", "corpus name")
	f.Var(newVersionIDFlag(&c.version), "version", "revert to a specific version")
	f.BoolVar(&c.safe, "safe", false, "revert to the most recent rollback-safe version")
}

func (c *setLiveCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	if c.name == "" {
		log.Printf("Must specify --name")
		return subcommands.ExitUsageError
	}
	if c.coordinatorURI == "" {
		log.Printf("Must specify --coordinator")
		return subcommands.ExitUsageError
	}
	if !c.safe && c.version.IsZero() {
		log.Printf("Must specify one of --version or --safe")
		return subcommands.ExitUsageError
	}

	err := c.Client().SetLiveVersion(ctx, c.name, c.version, c.safe)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&setLiveCommand{}, "")
}
