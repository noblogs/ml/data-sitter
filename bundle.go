package datasitter

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

var (
	checkTimeout          = 10 * time.Second
	downloadRetryInterval = 1 * time.Minute
	downloadTimeout       = 30 * time.Minute
)

// A Bundle is a collection of files tagged with a specific Version.
// Bundles are single-file compressed archives (current implementation
// is a Zstd-compressed Tar file).
type Bundle struct {
	Path    string
	Version *Version
}

// Unpack the bundle somewhere.
func (b *Bundle) unpack(dest string) error {
	// Shell out to native commands for performance.
	cmd := exec.Command("/bin/sh", "-c", fmt.Sprintf(
		"zstd -dc < '%s' | tar -xf -", b.Path))
	cmd.Dir = dest
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// Return the uncompressed data size.
func (b *Bundle) getSize() (int64, error) {
	// Shell out to native commands for performance.
	cmd := exec.Command("/bin/sh", "-c", fmt.Sprintf(
		"zstd -dc < '%s' | tar -tvf - | awk '{sum+=$3} END {print(sum)}'", b.Path))
	output, err := cmd.Output()
	if err != nil {
		return 0, err
	}
	return strconv.ParseInt(strings.TrimSpace(string(output)), 10, 64)
}

// Remove the bundle file.
func (b *Bundle) Remove() {
	os.Remove(b.Path)
	b.Version = nil
}

// MakeBundle creates a new Bundle. The resulting Bundle is incomplete
// (it lacks an attached Version) and is only meant for uploads to the
// coordinator.
func MakeBundle(src string) (*Bundle, error) {
	f, err := os.CreateTemp("", "bundle-*")
	if err != nil {
		return nil, err
	}
	defer f.Close()

	cmd := exec.Command("/bin/sh", "-c", "tar -cf - . | zstd -19 -c")
	cmd.Dir = src
	cmd.Stdout = f
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return nil, err
	}

	return &Bundle{
		Path: f.Name(),
	}, nil
}
