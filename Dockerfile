FROM docker.io/library/golang:1.20 AS build

ADD . /src
WORKDIR /src
RUN go build -trimpath -ldflags='-extldflags=-static -w -s' -tags osusergo,netgo,sqlite_omit_load_extension -o /data-sitter ./cmd/data-sitter

# The final image needs a shell and some tools to manage bundles.
FROM registry.git.autistici.org/pipelines/images/base/debian:bookworm
RUN apt-get update && \
    env DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends zstd tar && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*
COPY --from=build --chmod=0755 /data-sitter /data-sitter

ENTRYPOINT ["/data-sitter"]
