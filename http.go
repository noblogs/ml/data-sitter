package datasitter

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

// A HTTP downloader that can resume interrupted downloads using range
// requests.
type resumableGetter struct {
	*http.Client
}

func (g *resumableGetter) getSizeWithHeadRequest(ctx context.Context, uri, secret string) (int64, error) {
	req, err := http.NewRequestWithContext(ctx, "HEAD", uri, nil)
	if err != nil {
		return 0, err
	}
	setRequestAuth(req, secret)
	resp, err := g.Client.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("HTTP status %d", resp.StatusCode)
	}

	return strconv.ParseInt(resp.Header.Get("Content-Length"), 10, 64)
}

func (g *resumableGetter) ResumableGet(ctx context.Context, uri, secret, dest string) error {
	srcSize, err := g.getSizeWithHeadRequest(ctx, uri, secret)
	if err != nil {
		return fmt.Errorf("HEAD request to %s failed: %w", uri, err)
	}

	var dstSize int64
	if stat, err := os.Stat(dest); err == nil {
		dstSize = stat.Size()
	}

	if dstSize >= srcSize {
		return nil
	}

	// Make our ranged request.
	req, err := http.NewRequestWithContext(ctx, "GET", uri, nil)
	if err != nil {
		return err
	}
	setRequestAuth(req, secret)
	if dstSize > 0 {
		req.Header.Set("Range", fmt.Sprintf("%d-%d", dstSize, srcSize))
	}

	resp, err := g.Client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusPartialContent {
		return fmt.Errorf("HTTP status %d", resp.StatusCode)
	}

	df, err := os.OpenFile(dest, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0600)
	if err != nil {
		return err
	}

	_, err = io.Copy(df, resp.Body)
	return err
}

// Client for the coordinator API.
type Client struct {
	uri    string
	secret string
	getter *resumableGetter
}

func NewCoordinatorClient(uri, secret string) *Client {
	return &Client{
		uri:    strings.TrimRight(uri, "/"),
		secret: secret,
		getter: &resumableGetter{Client: new(http.Client)},
	}
}

func (c *Client) checkLatestVersion(ctx context.Context, corpus string, v *Version) (*Version, error) {
	values := make(url.Values)
	values.Set("corpus", corpus)
	if v != nil {
		values.Set("version", v.ID.String())
	} else {
		values.Set("version", "0")
	}
	uri := c.uri + "/api/check-version?" + values.Encode()

	req, err := http.NewRequestWithContext(ctx, "GET", uri, nil)
	if err != nil {
		return nil, err
	}
	setRequestAuth(req, c.secret)
	resp, err := c.getter.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusNotModified:
		return nil, nil

	case http.StatusOK:
		var newV Version
		if err := json.NewDecoder(resp.Body).Decode(&newV); err != nil {
			return nil, err
		}
		return &newV, nil

	default:
		return nil, fmt.Errorf("HTTP status code %d", resp.StatusCode)
	}
}

func (c *Client) downloadVersionToFile(ctx context.Context, corpus string, v *Version, dest string) error {
	values := make(url.Values)
	values.Set("corpus", corpus)
	values.Set("version", v.ID.String())
	uri := c.uri + "/api/download-version?" + values.Encode()
	return c.getter.ResumableGet(ctx, uri, c.secret, dest)
}

func buildMultipartRequest(corpus string, b *Bundle, setLive bool) (io.Reader, string, error) {
	f, err := os.Open(b.Path)
	if err != nil {
		return nil, "", err
	}
	defer f.Close()

	// Create the multipart request body.
	var buf bytes.Buffer
	writer := multipart.NewWriter(&buf)

	// Corpus field.
	if err := writer.WriteField("corpus", corpus); err != nil {
		return nil, "", err
	}

	if setLive {
		if err := writer.WriteField("live", "y"); err != nil {
			return nil, "", err
		}
	}

	part, err := writer.CreateFormFile("bundle", "bundle.tar.zstd")
	if err != nil {
		return nil, "", err
	}
	if _, err = io.Copy(part, f); err != nil {
		return nil, "", err
	}
	if err = writer.Close(); err != nil {
		return nil, "", err
	}

	return &buf, writer.FormDataContentType(), nil
}

func (c *Client) UploadNewVersion(ctx context.Context, corpus string, b *Bundle, setLive bool) (*Version, error) {
	input, contentType, err := buildMultipartRequest(corpus, b, setLive)
	if err != nil {
		return nil, fmt.Errorf("error building multipart request: %w", err)
	}

	// Issue the request and read the JSON response.
	req, err := http.NewRequestWithContext(ctx, "POST", c.uri+"/api/upload", input)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", contentType)
	setRequestAuth(req, c.secret)
	resp, err := c.getter.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP status code %d", resp.StatusCode)
	}

	var v Version
	if err := json.NewDecoder(resp.Body).Decode(&v); err != nil {
		return nil, err
	}
	return &v, nil
}

func (c *Client) ListVersions(ctx context.Context, corpus string) ([]Version, error) {
	values := make(url.Values)
	values.Set("corpus", corpus)

	req, err := http.NewRequestWithContext(ctx, "GET", c.uri+"/api/list-versions?"+values.Encode(), nil)
	if err != nil {
		return nil, err
	}
	setRequestAuth(req, c.secret)
	resp, err := c.getter.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP status code %d", resp.StatusCode)
	}

	var versions []Version
	if err := json.NewDecoder(resp.Body).Decode(&versions); err != nil {
		return nil, err
	}
	return versions, nil
}

func (c *Client) SetLiveVersion(ctx context.Context, corpus string, id VersionID, safe bool) error {
	values := make(url.Values)
	values.Set("corpus", corpus)
	if safe {
		values.Set("version", "safe")
	} else {
		values.Set("version", id.String())
	}
	req, err := http.NewRequestWithContext(ctx, "POST", c.uri+"/api/set-live-version", strings.NewReader(values.Encode()))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	setRequestAuth(req, c.secret)
	resp, err := c.getter.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("HTTP status code %d", resp.StatusCode)
	}
	return nil
}

func parseAuthHeaderFromRequest(req *http.Request) (string, bool) {
	if s := req.Header.Get("Authorization"); strings.HasPrefix(s, "Bearer ") {
		return s[7:], true
	}
	return "", false
}

func setRequestAuth(req *http.Request, secret string) {
	req.Header.Set("Authorization", "Bearer "+secret)
}
