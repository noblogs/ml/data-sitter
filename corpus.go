package datasitter

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"sync"
	"time"
)

type VersionID int64

func (i VersionID) IsZero() bool {
	return i == 0
}

func (i VersionID) String() string {
	if i.IsZero() {
		return "<NONE>"
	}
	return strconv.FormatInt(int64(i), 16)
}

func ParseVersionID(s string) (VersionID, error) {
	n, err := strconv.ParseInt(s, 16, 64)
	return VersionID(n), err
}

type Version struct {
	ID   VersionID `json:"id"`
	Size int64     `json:"size"`

	CreatedAt  time.Time `json:"created_at"`
	ReceivedAt time.Time `json:"received_at"`

	SetLiveAt  time.Time `json:"set_live_at"`
	RolledBack bool      `json:"rolled_back"`
}

func (v *Version) Less(other *Version) bool {
	if v == nil {
		return other != nil
	}
	if other == nil {
		return false
	}
	return v.ID < other.ID
}

type versionSort struct {
	l []*Version
}

func (s *versionSort) Len() int      { return len(s.l) }
func (s *versionSort) Swap(i, j int) { s.l[i], s.l[j] = s.l[j], s.l[i] }

// Sort versions by ID (descending).
type versionSortByID struct {
	*versionSort
}

func (s *versionSortByID) Less(i, j int) bool {
	return s.l[j].ID < s.l[i].ID
}

func newVersionSortByID(l []*Version) *versionSortByID {
	return &versionSortByID{
		versionSort: &versionSort{l: l},
	}
}

// Sort versions by SetLiveAt (descending).
type versionSortBySetLiveAt struct {
	*versionSort
}

func (s *versionSortBySetLiveAt) Less(i, j int) bool {
	return s.l[j].SetLiveAt.Before(s.l[i].SetLiveAt)
}

func newVersionSortBySetLiveAt(l []*Version) *versionSortBySetLiveAt {
	return &versionSortBySetLiveAt{
		versionSort: &versionSort{l: l},
	}
}

// A Corpus is a managed repository of Versions, with a "live" cursor.
type Corpus struct {
	dir        string
	unarchived bool
	mx         sync.Mutex

	Keep int `json:"keep"`

	Name string `json:"name"`

	Versions map[VersionID]*Version `json:"versions"`

	// To make this struct serialization-friendly, these are
	// references to the version map.
	Live VersionID `json:"live"`
}

func NewCorpus(name, dir string, unarchived bool) (*Corpus, error) {
	c := &Corpus{
		Name:       name,
		Versions:   make(map[VersionID]*Version),
		Keep:       2,
		dir:        dir,
		unarchived: unarchived,
	}

	if err := os.MkdirAll(dir, 0700); err != nil {
		return nil, err
	}
	if err := os.MkdirAll(c.tmpPath(), 0700); err != nil {
		return nil, err
	}

	if err := c.loadState(); err != nil && !os.IsNotExist(err) {
		log.Printf("warning: could not load corpus state: %v", err)
	}

	return c, nil
}

func (c *Corpus) tmpPath() string {
	return filepath.Join(c.dir, ".tmp")
}

func (c *Corpus) stateFilePath() string {
	return filepath.Join(c.dir, ".state.json")
}

func (c *Corpus) versionPath(v *Version) string {
	return filepath.Join(c.dir, fmt.Sprintf("version-%s", v.ID.String()))
}

func (c *Corpus) saveState() error {
	tmpf := c.stateFilePath() + ".tmp"
	defer os.Remove(tmpf)

	f, err := os.Create(tmpf)
	if err != nil {
		return err
	}
	defer f.Close()
	if err := json.NewEncoder(f).Encode(c); err != nil {
		return err
	}
	return os.Rename(tmpf, c.stateFilePath())
}

func (c *Corpus) loadState() error {
	f, err := os.Open(c.stateFilePath())
	if os.IsNotExist(err) {
		return nil
	}
	if err != nil {
		return err
	}
	defer f.Close()
	return json.NewDecoder(f).Decode(c)
}

func (c *Corpus) withReadonlyTx(f func() error) error {
	c.mx.Lock()
	defer c.mx.Unlock()
	return f()
}

func (c *Corpus) withTx(f func() error) error {
	c.mx.Lock()
	defer c.mx.Unlock()

	err := f()

	// If all went well, attempt to clean up.
	if err == nil {
		err = c.cleanup()
	}

	// Always save state. If f has returned an error, keep that.
	serr := c.saveState()
	if err == nil && serr != nil {
		err = fmt.Errorf("failed to update corpus state: %w", serr)
	}

	return err
}

func (c *Corpus) containsVersion(v *Version) bool {
	_, ok := c.Versions[v.ID]
	return ok
}

func (c *Corpus) getVersionByID(id VersionID) (*Version, bool) {
	v, ok := c.Versions[id]
	return v, ok
}

func (c *Corpus) getLiveVersion() (Version, bool) {
	if c.Live.IsZero() {
		return Version{}, false
	}
	v := c.Versions[c.Live]
	return *v, true
}

// Useful to take values outside of the lock (for HTML rendering and
// other similar "unlocked" tasks).
func derefVersions(versions []*Version) []Version {
	out := make([]Version, 0, len(versions))
	for _, v := range versions {
		out = append(out, *v)
	}
	return out
}

func (c *Corpus) getAllVersions() []*Version {
	versions := make([]*Version, 0, len(c.Versions))
	for _, v := range c.Versions {
		versions = append(versions, v)
	}
	return versions
}

func (c *Corpus) getAllVersionsSortedByID() []*Version {
	versions := c.getAllVersions()
	sort.Sort(newVersionSortByID(versions))
	return versions
}

func (c *Corpus) getAllVersionsSortedBySetLiveAt() []*Version {
	versions := c.getAllVersions()
	sort.Sort(newVersionSortBySetLiveAt(versions))
	return versions
}

func (c *Corpus) accept(b *Bundle) error {
	if c.containsVersion(b.Version) {
		return nil
	}

	// Copy / move / extract the bundle into the local storage.
	var err error
	if c.unarchived {
		path := c.versionPath(b.Version)
		err = os.MkdirAll(path, 0750)
		if err == nil {
			err = b.unpack(path)
		}
	} else {
		err = renameOrCopy(b.Path, c.versionPath(b.Version))
	}
	if err != nil {
		return err
	}

	c.Versions[b.Version.ID] = b.Version

	return nil
}

// Move the 'live' cursor.
func (c *Corpus) setLiveVersion(id VersionID) error {
	v, ok := c.Versions[id]
	if !ok {
		return fmt.Errorf("no such version %s", id)
	}

	// Are we rolling back?
	if !c.Live.IsZero() && id < c.Live {
		if old, ok := c.Versions[c.Live]; ok {
			old.RolledBack = true
		}
	}

	// Mark the new session as the active one, and keep track of
	// the rollback-safe version (the one we were currently
	// running successfully).
	v.SetLiveAt = time.Now()
	c.Live = id

	return nil
}

// Find the most recent rollback-safe version.
func (c *Corpus) getRollbackSafeVersion(sortedVersions []*Version) (VersionID, bool) {
	if sortedVersions == nil {
		sortedVersions = c.getAllVersionsSortedByID()
	}
	for _, v := range sortedVersions {
		if !v.SetLiveAt.IsZero() && !v.RolledBack && v.ID != c.Live {
			return v.ID, true
		}
	}
	return 0, false
}

func (c *Corpus) cleanup() error {
	keepIDs := make(map[VersionID]struct{})
	versions := c.getAllVersionsSortedByID()

	//log.Printf("cleanup started, %d versions present", len(versions))

	// Always keep the live version, and the safe rollback (do not
	// count these towards the total).
	if !c.Live.IsZero() {
		keepIDs[c.Live] = struct{}{}
		//log.Printf("keeping version %s because it is live", c.Live)
	}
	if id, ok := c.getRollbackSafeVersion(versions); ok {
		keepIDs[id] = struct{}{}
		//log.Printf("keeping version %s because it is rollback-safe", id)
	}

	// Keep the last N versions that were set live at some point.
	count := 0
	for _, v := range c.getAllVersionsSortedBySetLiveAt() {
		if count >= c.Keep {
			break
		}
		keepIDs[v.ID] = struct{}{}
		//log.Printf("keeping version %s because it was live recently (count=%d)", v.ID, count)
		count++
	}

	for _, v := range versions {
		// Version marked to be kept?
		if _, ok := keepIDs[v.ID]; ok {
			continue
		}

		// Do not drop anything that is more recent than the live version.
		if v.ID > c.Live {
			//log.Printf("keeping version %s because it is more recent than live version O%s", v.ID, c.Live)
			continue
		}

		if count < c.Keep {
			count++
			continue
		}

		log.Printf("cleaning up obsolete version %s", v.ID.String())
		delete(c.Versions, v.ID)
		os.RemoveAll(c.versionPath(v))
	}

	return nil
}

// Rename or copy src to dst.
func renameOrCopy(src, dst string) error {
	// Try os.Rename first, will succeed if source and destination
	// are on the same filesystem.
	if err := os.Rename(src, dst); err == nil {
		return nil
	}

	// Simple golang stdlib file copy.
	fsrc, err := os.Open(src)
	if err != nil {
		return err
	}
	defer fsrc.Close()

	fdst, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer fdst.Close()

	if _, err := io.Copy(fdst, fsrc); err != nil {
		return err
	}

	return os.Remove(src)
}
